const express = require('express');
const router = express.Router();

const { getLogs } = require('./service.js');

router.get('/', getLogs);

module.exports = {
    logsRouter: router
};
