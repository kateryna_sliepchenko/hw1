const fs = require('fs');
const express = require('express');
const morgan = require('morgan')
const app = express();

const {filesRouter} = require('./filesRouter.js');
const {logsRouter} = require('./logger/router.js');
const Logger = require('./logger');

app.use(express.json());
app.use(morgan('tiny'));

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

// QUESTION: why we are using POST:/api/files to create single file?
app.use('/api/files', filesRouter);
app.use('/api/logs', logsRouter);

const start = async () => {
    try {
        if (!fs.existsSync('files')) {
            fs.mkdirSync('files');
        }
        
        app.listen(8080);
        Logger.log('Application initialized');
    } catch (err) {
        console.error(`Error on server startup: ${err.message}`);
    }
}

start();

//ERROR HANDLER
app.use(errorHandler)


function errorHandler(err, req, res) {
    console.error('err')
    res.status(500).send({'message': 'Server error'});
}
